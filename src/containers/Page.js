import React, { Component } from 'react';
import CarouselKu from '../components/CarouselKu';
import SearchKu from '../components/SearchKu';
class Page extends Component {
	render() {
		return (
			<div>
				<SearchKu/>
				<CarouselKu/>
			</div>
		);
	}
}

export default Page;