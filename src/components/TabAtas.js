import { Tabs, WhiteSpace } from 'antd-mobile';
import React from 'react';
import Page from '../containers/Page';

const tabs = [
  { title: 'First Tab' },
  { title: 'Makanan' },
  { title: 'Third Tab' },
];

const TabAtas = () => (
  <div>
    <WhiteSpace />
    <div style={{ height: 'auto' }}>
      <Tabs tabs={tabs}
        initalPage={'t2'}
        swipeable={false}
      >
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '250px', backgroundColor: '#fff' }}>
          Content of first tab
        </div>
        <div >
          <Page/>
        </div>
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '250px', backgroundColor: '#fff' }}>
          Content of third tab
        </div>
      </Tabs>
    </div>
  </div>
);

export default TabAtas