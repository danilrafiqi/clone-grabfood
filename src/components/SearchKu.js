import React from 'react';
import { SearchBar, WhiteSpace } from 'antd-mobile';

class SearchKu extends React.Component {
  state = {
    value: 'close',
  };

  onChange= (value) => {
    this.setState({ value });
  };
  clear = () => {
    this.setState({ value: '' });
  };

  render() {
    return (<div>
      <SearchBar placeholder="Mau makan apa?" maxLength={8} cancelText={'Close'} />
      <WhiteSpace />

    </div>);
  }
}

export default SearchKu;