import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';
import TabAtas from './components/TabAtas';

class App extends Component {
  render() {
    return (
      <div>
      <Navbar/>
      <TabAtas/>
    </div>
    );
  }
}

export default App;
